const fs = require('fs');
const exec  = require('child_process').exec;
const path = require('path')

//https://stackoverflow.com/questions/1880198/how-to-execute-shell-command-in-javascript
async function sh(cmd) {
    return new Promise(function (resolve, reject) {
      exec(cmd, (err, stdout, stderr) => {
        if (err) {
          reject(err);
        } else {
          resolve({ stdout, stderr });
        }
      });
    });
  }


class Application
{
    constructor(appObj)
    {
        var _appObj = appObj;
        this.app = function app()
        {
            
            return _appObj;
        }
        this.pid = _appObj.pid;
        this.name = _appObj.name;
        this.packageName = _appObj.identifier;

    }
    async downloadApp(devID,downloadPath)
    {
        let {stdout} = await sh(`adb -s ${devID} shell pm path ${this.packageName}`);
        let APKpath = (stdout.match(/package:(.+)/))[1]
        
        let destinationPath = path.join(downloadPath,this.packageName);
        // if(downloadPath.charAt(downloadPath.length - 1) != '/')
        //     downloadPath += '/';
        // downloadPath += this.packageName;
        try 
        {
          if(!fs.existsSync(destinationPath))
          {
            fs.mkdirSync(destinationPath);
          }
          var stdoutDownload = await sh(`adb -s ${devID} pull ${APKpath} ${destinationPath}`);
          return {
            message:`Download "${this.packageName}" to "${destinationPath}" done!`,
            path:destinationPath
          };
        } 
        catch (error)
        {
            throw error;
        }
    }
    getDeeplinks()
    {

    }

}
module.exports = 
{
    Application
}