const frida = require('frida');
const { Application } = require("./applications");
// const exec = require('child_process');
var fs = require('fs')
var path = require('path')
var util = require('util')
const readFile = util.promisify(fs.readFile);
const source = `
rpc.exports = {
    checkJavaAPI : function() {
        return Java.available;
    }
}
`
class Device {
    constructor(deviceObj) {
        var _deviceObj = deviceObj;
        this.device = function device() {
            return _deviceObj;
        }
        this.id = _deviceObj.id;
        this.name = _deviceObj.name;
        this.type = _deviceObj.type;
        this.icon = _deviceObj.icon;

        //TODO: Add initializer for these
        this.isRooted = null;
        this.architecture = null;
        this.androidVersion = null;



    }
    async getFrontmostApplication() {
        const frontApp = await this.device().getFrontmostApplication();
        return new Application(frontApp);
    }
    async getAllApplications() {
        const applications = await this.device().enumerateApplications();
        const frontApp = await this.getFrontmostApplication();
        const applicationsInstance = applications
            .map(app => {
                if (app.identifier != frontApp.identifier)
                    return new Application(app);
            }).sort((a, b) => {
                if (a.name < b.name) return -1;
                if (a.name > b.name) return 1;
                return 0;
            })
        return {
            front: frontApp,
            apps: applicationsInstance
        };
    }
    async getApplication(appPackageName) {
        const applications =( await this.getAllApplications()).apps;
        if (applications.length > 0) {
            const application = applications.find(app => app.packageName === appPackageName);
            // this.launchApp();
            return application;
        }
        return null;
    }
    async launchApp(appPackageName) {
        const app = await this.getApplication(appPackageName);
        if (app.pid == 0) {
            const pid = await this.device().spawn([appPackageName]);
            // await this.device().resume(pid);
            app.pid = pid;
        }
        return app;
    }
    async terminateApp(appPackageName) {
        const app = await this.getApplication(appPackageName);
        const pid = app.pid;
        await this.device().kill(pid);
    }
    async attachToApp(appPackageName) {
        const app = await this.getApplication(appPackageName);
        const pid = app.pid;
        const session = this.device().attach(pid);
        return session;
    }
    async loadScript(appPackageName, selectedScript) {
        const session = await this.attachToApp(appPackageName);
        const script = await session.createScript(selectedScript);
        await script.load();
        return script;
    }
    async checkJavaAPI(appPackageName) {
        console.log(appPackageName);

        const sourceRPC = await readFile(path.join(__basedir, 'agent','rpc', 'application-rpc.js'),'utf8')
        const loadedScript = await this.loadScript(appPackageName, sourceRPC);
        const api = loadedScript.exports;
        const isJavaAvailable = api.checkJavaAPI;
        return isJavaAvailable;
    }
    async loadFileRPC(appPackageName) {
        const sourceRPC = await readFile(path.join(__basedir,'agent','rpc', 'file-rpc.js'), 'utf8');
        const loadedScript = await this.loadScript(appPackageName, sourceRPC);
        return loadedScript;
    }
    async getAppDataPath(appPackageName) {
        const loadedScript = await this.loadFileRPC(appPackageName);
        try {
            const api = loadedScript.exports;
            const appDataPath = await api.getAppDataPath();
            return appDataPath;
        }
        catch (error) {
            throw error;
        }
        finally {
            await loadedScript.unload();
        }

    }
}
async function getAllDevices() {
    const allDevices = await frida.enumerateDevices();
    const duriandroidDevices = allDevices
        .filter(device => {
            return device.impl.type == 'usb';
        })
        .map(device => {
            var deviceObj = new Device(device);
            return deviceObj;
        });
    return duriandroidDevices;
}

async function getDevice(deviceID) {
    const allDevices = await getAllDevices();
    if (allDevices.length > 0) {
        const device = allDevices.find(device => device.id === deviceID);
        return device;
    }
    return null;
}

module.exports =
{
    Device,
    getAllDevices,
    getDevice,
}