function getClass(selectedClassName)
{
    if (Java.available)
    {
        var appClass = {};
        Java.perform( function ()
        {
            try {
                Java.enumerateLoadedClasses(
                    {
                        onMatch: function(className)
                        {
                            if(className == selectedClassName)
                            {
                                try {
                                    appClass = {'name' : className};
                                    var _class = Java.use(className);
                                    var _Modifier = Java.use('java.lang.reflect.Modifier');
                                    var classConstructors = _class.class.getDeclaredConstructors();
                                    var _ctor = [];
                                    for (var idx in classConstructors) {
                                        var classConstructor = classConstructors[idx];
                                        var constructorObj = {};
                                        constructorObj['name'] = classConstructor.getName(),
                                        constructorObj['fullname'] = classConstructor.toString()
                                        constructorObj['modifier'] = _Modifier.toString(classConstructor.getModifiers())
                                        constructorObj['params'] = {}; //use dict since it counts from arg0, arg1, arg2, ...
                                        var constructorParams = classConstructor.getParameters();
                                        var params = [];
                                        for (var key in constructorParams) 
                                        {
                                            var param = constructorParams[key];
                                            var paramObj = {};
                                            // paramObj['fullname'] = param.toString();
                                            paramObj['name'] = param.getName();
                                            paramObj['type'] = (param.getType().toString()).replace(/\w+\s+/g,'');
                                            console.log(JSON.stringify(paramObj));
                                            
                                            params.push(paramObj);
                                            
                                        }
                                        
                                        constructorObj['params'] = params;
                                        _ctor.push(constructorObj);
                                    }
                                    appClass['constructors'] = _ctor;
                                    
                                    var classMethods = _class.class.getDeclaredMethods();
                                    var classFields = _class.class.getDeclaredFields();

                                    var fields = [];
                                    var methods = [];

                                    var fieldObj = {};
                                    for (var key in classFields) 
                                    {
                                        var field = classFields[key];
                                        fieldObj['fullname'] = field.toString();
                                        fieldObj['name'] = field.getName();
                                        fieldObj['modifier'] = _Modifier.toString(field.getModifiers());
                                        fieldObj['type'] = (field.getDeclaringClass().toString()).replace(/\w+\s+/g,'');
                                        
                                        fields.push(fieldObj); 
                                        field.$dispose();
                                    }
                                    appClass['fields'] = fields;
                                    var methodObj = {};
                                    for (var key in classMethods) 
                                    {
                                        var method = classMethods[key];
                                        var methodParams = method.getParameters();
                                        var methodObj = {};
                                        
                                        methodObj['fullname'] = method.toString();
                                        methodObj['name'] = method.getName();
                                        methodObj['modifier'] = _Modifier.toString(method.getModifiers());
                                        methodObj['returnType'] = (method.getReturnType().toString()).replace(/\w+\s+/g,'');
                                        
                                        methodObj['params'] = {}; //use dict since it counts from arg0, arg1, arg2, ...
                                        var params = [];
                                        for (var key in methodParams) 
                                        {
                                            var param = methodParams[key];
                                            var paramObj = {};
                                            methodObj['params'] = []
                                            // paramObj['fullname'] = param.toString();
                                            paramObj['name'] = param.getName();
                                            paramObj['type'] = (param.getType().toString()).replace(/\w+\s+/g,'');
                                            params.push(paramObj);
                                            
                                        }
                                        methodObj['params'] = params;
                                        methods.push(methodObj);

                                    }
                                    appClass['methods'] = methods;
                                    _class.$dispose()
                                } catch (error) {
                                    console.log(error);
                                    throw new Error(error);
                                }
                            }
                        },
                        onComplete : function(){}
                    }
                )
            } catch (error) {
                console.error(error);
                throw new Error(error);
            }
        });
        return appClass;
    }
}
function enumerateClasses()
{
    var classes = []
    if(Java.available)
    {
        Java.perform(function () 
        {
            try {
                Java.enumerateLoadedClasses(
                {
                    onMatch: function(className)
                    {
                        try {
                            classes.push({'name':className});
                            
                        } catch (error) {
                            console.error(error)
                        }
                    },
                    onComplete: function(){}
                });
            } catch (error) {
                console.error(error);
                throw new Error(error);
            }
        });
    }
    return classes;
}

rpc.exports = 
{
    getClass,
    enumerateClasses,
}