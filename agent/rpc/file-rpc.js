function getAppDataPath()
{
    return new Promise(function (resolve,reject)
    {
        Java.perform(function ()
        {
            try {
                var File = Java.use('java.io.File');
                var Application = Java.use('android.app.ActivityThread');
                var Context = Application.currentApplication().getApplicationContext();
                var ApplicationInfo = Context.getApplicationInfo();
                var fileDir = Context.getDataDir();
                var pwd = fileDir.getAbsolutePath();
                resolve(pwd);
            } catch (error) {
                reject(error)
            }
        })
    })
}
function getAppData()
{
    return new Promise(function (resolve,reject) 
        {
            Java.perform(function()
                {
                    try 
                    {
                        var File = Java.use('java.io.File');
                        var Application = Java.use('android.app.ActivityThread');
                        var Context = Application.currentApplication().getApplicationContext();
                        var ApplicationInfo = Context.getApplicationInfo();
                        console.log(ApplicationInfo.dataDir.value)
                        // var packageName = Context.getPackageName();
                        var fileDir = Context.getDataDir();
                        // var desirePath = '/data/data/'+packageName;
                        // var pathFile = File.$new.overload('java.lang.String','java.lang.String').call(File,desirePath,path);
                        var pwd = fileDir.getAbsolutePath();
                        var currentDirFile = fileDir.listFiles();
                        var currentDirJSON = {
                            'pwd':pwd,
                            'ls':
                                []
                        };
                        currentDirJSON['ls'].push({
                            'name' : '..',
                            'type' : 'D',
                            'path' : fileDir.getParent()
                        })
                        currentDirFile.map(function(file){
                            var fileObj = {'name' : file.getName() , 'path' : file.getAbsolutePath()}
                            if(file.isDirectory)
                            {
                                fileObj['type'] = 'D';
                            }
                            else
                            {
                                fileObj['type'] = 'F';
                            }
                            fileObj['permission'] = {'r' :file.canRead(),'w':file.canWrite(),'x':file.canExecute()}
                            currentDirJSON['ls'].push(fileObj);
                            return file;
                        })
                        resolve(currentDirJSON);
                    } 
                    catch (error) 
                    {
                        reject(error);
                    }
                }	
            );
        });
}
function getPathData(path)
{
    var directory = null;
    return new Promise(function (resolve,reject) 
        {
            Java.perform(function()
                {
                    try 
                    {
                        var File = Java.use('java.io.File');
                        var Application = Java.use('android.app.ActivityThread');
                        var Context = Application.currentApplication().getApplicationContext();
                        // var packageName = Context.getPackageName();
                        
                        var pathFile = File.$new.overload('java.lang.String').call(File,path);
                        var pwd = pathFile.getAbsolutePath();
                        var currentDirFile = pathFile.listFiles();
                        if(!pathFile.canRead())
                        {
                            throw new Error('Permission denied at "'+pwd+'"')
                        }
                        
                        var currentDirJSON = {
                            'pwd':pwd,
                            'ls':
                                []
                        };
                        currentDirJSON['ls'].push({
                            'name' : '..',
                            'type' : 'D',
                            'path' : pathFile.getParent()
                        })
                        currentDirFile.map(function(file){
                            var fileObj = {'name' : file.getName(),'path' : file.getAbsolutePath()}
                            if(file.isDirectory)
                            {
                                fileObj['type'] = 'D';
                            }
                            else
                            {
                                fileObj['type'] = 'F';
                            }
                            fileObj['permission'] = {'r' :file.canRead(),'w':file.canWrite(),'x':file.canExecute()}
                            currentDirJSON['ls'].push(fileObj);
                            return file;
                        })
                        
                        resolve(currentDirJSON);
                    } 
                    catch (error) 
                    {
                        reject(error);
                    }
                }	
            );
        });
}
rpc.exports = {
    getAppDataPath,
    getAppData,
    getPathData
}