function getInfo()
{
    var result;
    if(Java.available)
    {
        Java.perform(function()
        {
            //Permission
            var permissions = [];
            var Application = Java.use('android.app.ActivityThread');
            var Context = Application.currentApplication().getApplicationContext();

            var PackageManager = Context.getPackageManager();
            var packageName = Context.getPackageName();
            var String = Java.use('java.lang.String')
            // send(PackageManager.GET_PERMISSIONS);
            
            var PackageInfo = PackageManager.getPackageInfo(packageName,4096);
            var requestedPermissions = PackageInfo.requestedPermissions;

            if(requestedPermissions != null)
            {
                for(var key in requestedPermissions.value)
                {
                    var permission = requestedPermissions.value[key];
                    permissions.push(permission);
                }
            }

            //SDK Version
    
            var SDKVersion = 
            {
                'min':'',
                'target':''
            };
            var ApplicationInfo = Context.getApplicationInfo();
            var Integer = Java.use('java.lang.Integer')
            SDKVersion.min = Integer.$new(ApplicationInfo.minSdkVersion.value).toString()
            SDKVersion.target = Integer.$new(ApplicationInfo.targetSdkVersion.value).toString()
            
            //version 
            var version = PackageInfo.versionName.value;
            result = {
                permissions,
                SDKVersion,
                version
            }
        })
    }

    return result;
}



rpc.exports = {
    getInfo
}