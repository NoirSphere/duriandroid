global.__basedir = __dirname;
const Koa = require('koa');
const Router = require('koa-router');
const cors = require('@koa/cors')
const bodyParser = require('koa-body');
const { getAllDevices, getDevice } = require('./agent/devices/devices');
const app = new Koa();
const api = new Router({ prefix: '/api' });
const serve = require('koa-static');
const logger = require('koa-logger')
const mount = require('koa-mount')
const send = require('koa-send')
const path = require('path')
const http = require('http').createServer(app.callback());
var io = require('./io/main.js')

const device = new Router();
const application = new Router();
const file = new Router();


app.on('error', err => {
    console.error('server error', err);
});

//Set up body parsing middleware
app.use(bodyParser({
    formidable: {
        keepExtensions: true,
    },    //This is where the files would come
    multipart: true,
    urlencoded: true
}));

file
    // .get('/', async (ctx) => {
    //     const appPackageName = ctx.params.appPackageName;
    //     const deviceID = ctx.params.devID;
    //     const device = await getDevice(deviceID);
    //     console.log(appPackageName);
        
    //     const appData = await device.getAppData(appPackageName);
    //     ctx.body = appData;
    // })
    .get('/', async (ctx) => {
        const appPackageName = ctx.params.appPackageName;
        const deviceID = ctx.params.devID;
        const device = await getDevice(deviceID);
        console.log(appPackageName);
        
        const appDataPath = await device.getAppDataPath(appPackageName);
        ctx.body = appDataPath;
    })
    .post('/', async (ctx) => {
        const appPackageName = ctx.params.appPackageName;
        const deviceID = ctx.params.devID;
        const path = ctx.request.body.path;
        const device = await getDevice(deviceID);
        const pathData = await device.getPathData(appPackageName, path);
        ctx.body = pathData;
    })
application
    .get('/', async (ctx) => {
        const deviceID = ctx.params.devID;
        const device = await getDevice(deviceID);
        const { front, apps } = await device.getAllApplications();
        ctx.body =
        {
            front,
            apps
        };
    })
    .get('/:appPackageName', async (ctx) => {
        const appPackageName = ctx.params.appPackageName;
        const deviceID = ctx.params.devID;
        const device = await getDevice(deviceID);
        // const isJavaAPIAvailable = await device.checkJavaAPI(appPackageName);
        // console.log('java',isJavaAPIAvailable);
        // if(isJavaAPIAvailable)
        // {
        const app = await device.launchApp(appPackageName);
        ctx.body = app;
        // }
        // else
        // {
        // console.log('error');

        //     ctx.status = 500
        //     ctx.body = 'Java API not available'
        // }
    })
    .delete('/:appPackageName', async (ctx) => {
        const appPackageName = ctx.params.appPackageName;
        const deviceID = ctx.params.devID;
        const device = await getDevice(deviceID);
        const app = await device.getApplication(appPackageName);
        await device.terminateApp(appPackageName);
        ctx.body = { 'result': `${appPackageName} has been terminated` }
    })
    .post('/:appPackageName/download', async (ctx) => {
        const appPackageName = ctx.params.appPackageName;
        const deviceID = ctx.params.devID;
        const device = await getDevice(deviceID);
        const app = await device.getApplication(appPackageName);
        const downloadPath = ctx.request.body.downloadPath;
        try {
            ctx.body = { 'result': await app.downloadApp(deviceID,downloadPath) };
        } catch (error) {
            ctx.status = 400;
            ctx.body = error.message
        }
    })
    .use('/:appPackageName/files', file.routes(), file.allowedMethods())
device
    .get('/', async (ctx) => {
        const allDevices = await getAllDevices();
        ctx.body =
        {
            allDevices
        };
    })
    .use('/:devID/applications', application.routes(), application.allowedMethods())
api
    .get('/', async (ctx) => {
        ctx.body = 'Duriandroid!';
    })
    .use('/devices', device.routes(), device.allowedMethods());


app
    .use(cors({ origin: '*' }))
    .use(api.routes())
    .use(api.allowedMethods())
    .use(async (ctx, next) => {
        if (process.env.NODE_ENV === 'development') {
            next()
        } else {
            const opt = { root: path.join(__dirname, 'client') }
            if (ctx.path.startsWith('/static/')) await send(ctx, ctx.path, opt)
            else await send(ctx, '/static/index.html', opt)
            next()
        }
    })
    .use(logger())


// start(1337)
function start(port) {
    console.log(`server start at http://127.0.0.1:${port}`)
    const server = http
    io.attach(server)
    server.listen(port,"localhost")
}

module.exports = {
    start
}