
const monacoWebpackPlugin = require('monaco-editor-webpack-plugin');

module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/static'
    : '/',
  "transpileDependencies": [
    "vuetify"
  ],
  outputDir: 'static',
  devServer: {
    host: "localhost",
    proxy: {
      "/api": {
        target: "http://localhost:1337",
      },
      "/io": {
        target: "http://localhost:1337",
      }
    }
  },
  configureWebpack: {
    plugins: [
      new monacoWebpackPlugin()
    ],
    devtool: 'source-map'
  },
}