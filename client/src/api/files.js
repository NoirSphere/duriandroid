const {api} = require('./base')

function getAppData(devID,packageName)
{
    return api.get(`/devices/${devID}/applications/${packageName}/files`)
}

function goToPath(devID,packageName,desirePath)
{
    return api.post(`/devices/${devID}/applications/${packageName}/files`,
    {
        path: desirePath
    })
}

module.exports = {
    getAppData,
    goToPath
}