const {api} = require('./base')
function getUSBDevices()
{
    return api.get('/devices');
}


module.exports = {
    getUSBDevices,
}