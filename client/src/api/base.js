const axios = require('axios');
export const api = axios.create({
    baseURL : '/api',
    // timeout:10000,
    headers: {
        'Access-Control-Allow-Origin' : '*'
    }
})
