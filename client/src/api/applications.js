const { api } = require('./base')

function getAllApplications(id) {
    return api.get(`/devices/${id}/applications`)
}
function downloadApplication(devID, packageName, path) {
    return api.post(`/devices/${devID}/applications/${packageName}/download`,
        {
            downloadPath: path
        })
}

function launchApplication(devID, packageName) {
    return api.get(`/devices/${devID}/applications/${packageName}`)
}

function terminateApplication(devID, packageName) {
    return api.delete(`/devices/${devID}/applications/${packageName}`)
}


module.exports = {
    getAllApplications,
    downloadApplication,
    launchApplication,
    terminateApplication
}