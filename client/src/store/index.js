import Vue from 'vue'
import Vuex from 'vuex'
import application from "./application/index";

Vue.use(Vuex)

const store =  new Vuex.Store({
  modules :{
    application
  }
})

export default store;