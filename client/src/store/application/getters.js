import { GET_APP, GET_SOCKET, IS_HOOK, IS_MODIFY_HOOK, LOGS, UNREAD_COUNT, MODIFY_HOOKS, HOOKS,SYSTEM_SCRIPTS } from "./getter-types";

export default {
    [GET_SOCKET](state) {
        return state.socket;
    },
    [GET_APP](state) {
        return state.app;
    },
    [IS_HOOK]: (state) => (_class, targetFullname) => {
        if (state.hooks[_class] && state.hooks[_class][targetFullname]) {
            return state.hooks[_class][targetFullname]
        }
        else {
            return false;
        }
    },
    [IS_MODIFY_HOOK]: (state) => (_class, targetFullname) => {
        if (state.modifyHooks[_class] && state.modifyHooks[_class][targetFullname]) {
            return state.modifyHooks[_class][targetFullname]
        }
        else {
            return false;
        }
    },
    [HOOKS](state) {
        return state.hooks
    },
    [MODIFY_HOOKS](state) {
        return state.modifyHooks
    },
    [LOGS](state) {
        return state.logs.sort((log_a,log_b)=>log_a.timestamp-log_b.timestamp)

    },
    [UNREAD_COUNT](state) {
        return state.unread
    },
    [SYSTEM_SCRIPTS](state)
    {
        return state.systemScripts;
    }
}