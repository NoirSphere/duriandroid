import mutations from "./mutations";
import getters from "./getters";
import actions from "./actions";

export default {
    state : {
        socket: null,
        app:{
            name:'',
            pid:0,
            packageName:''
        },
        hooks:{},
        modifyHooks:{},
        systemScripts:[],
        console:[''],
        logs:[],
        unread:0,
        consoleActive:false
    },
    mutations,
    getters,
    actions
}