import { STORE_SOCKET,STORE_APP, STORE_HOOK_METHOD, REMOVE_HOOK_STATE, REMOVE_HOOK_MODIFY_STATE, STORE_HOOK_MODIFY, STORE_HOOK_CTOR,ADD_LOG,CLEAR_LOG,OPEN_CONSOLE,CLOSE_CONSOLE,CLEAR_LOG_BY_SCRIPT_TAG,REMOVE_SYSTEM_SCRIPT } from "./mutation-types";
import Vue from "vue";

const LIMIT = 100;

export default
{
    [STORE_SOCKET](state,socket)
    {
        //from passionfruit
        socket.call = (function (event,...data){
            return new Promise((resolve,reject) => {
                let ok =  false;
                this.emit(event,data,response => {
                    if(response.status === 'ok')
                    {
                        ok = true;
                        resolve(response.data)
                    } else {
                        reject(response.error)
                    }
                })

                setTimeout(()=>{
                    if(!ok)
                        reject('Request timed out')
                },5000)
            })
        }).bind(socket)
        state.socket = socket;
    },
    [STORE_APP](state,app)
    {
        state.app = app
    },
    [STORE_HOOK_METHOD](state,args)
    {
        const {_class,methodFullname} = args;
        if(state.hooks.hasOwnProperty(_class))
            Vue.set(state.hooks[_class],methodFullname,true);
        else
        {
            Vue.set(state.hooks,_class,{
                [methodFullname] : true
            })
        }
    },
    [STORE_HOOK_CTOR](state,args)
    {
        const {_class,ctorFullname} = args;
        if(state.hooks.hasOwnProperty(_class))
            Vue.set(state.hooks[_class],ctorFullname,true);
        else
        {
            Vue.set(state.hooks,_class,{
                [ctorFullname] : true
            })
        }
    },
    [REMOVE_HOOK_STATE](state,args)
    {
        const {_class,targetFullname} = args;
        Vue.delete(state.hooks[_class],targetFullname);
        delete state.hooks[_class][targetFullname];
        if(Object.keys(state.hooks[_class]).length <= 0)
        {
            Vue.delete(state.hooks,_class);
        }
    },
    [STORE_HOOK_MODIFY](state,args)
    {
        const {_class,targetFullname} = args;
        if(state.modifyHooks.hasOwnProperty(_class))
            Vue.set(state.modifyHooks[_class],targetFullname,true);
        else
        {
            Vue.set(state.modifyHooks,_class,{
                [targetFullname] : true
            })
        }
    },
    [REMOVE_HOOK_MODIFY_STATE](state,args)
    {
        const {_class,targetFullname} = args;
        Vue.delete(state.modifyHooks[_class],targetFullname);
        delete state.modifyHooks[_class][targetFullname];
        if(Object.keys(state.modifyHooks[_class]).length <= 0)
        {
            Vue.delete(state.modifyHooks,_class);
        }
    },
    [ADD_LOG](state,args)
    {
        state.logs.unshift({
            ...args
        });
        if(state.logs.length > LIMIT)
            state.logs.pop()

        if(!state.active)
            state.unread++  

    },
    [CLEAR_LOG](state)
    {
        state.unread = 0;
        state.logs = [];
    },
    [CLEAR_LOG_BY_SCRIPT_TAG](state,tag)
    {
        state.logs = state.logs.filter(log=>log.script !== tag);
    },
    [OPEN_CONSOLE](state)
    {
        state.unread = 0;
        state.consoleActive = true
    },
    [REMOVE_SYSTEM_SCRIPT](state,script)
    {
        let index =state.systemScripts.findIndex(sc => sc === script);
        if(index > -1)
            Vue.delete(state.systemScripts,index)
    },
    [CLOSE_CONSOLE](state)
    {
        state.consoleActive = false
    },
}