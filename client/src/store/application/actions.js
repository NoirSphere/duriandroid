import { HOOK_METHOD,HOOK_CTOR,UNHOOK,MODIFY_METHOD,UNHOOK_MODIFY,UNLOAD_SYSTEM_SCRIPT,FETCH_SYSTEM_SCRIPTS  } from "./action-types";
import { STORE_HOOK_METHOD,STORE_HOOK_CTOR,REMOVE_HOOK_STATE,STORE_HOOK_MODIFY,REMOVE_HOOK_MODIFY_STATE, REMOVE_SYSTEM_SCRIPT } from "./mutation-types";

export default {
    async [HOOK_METHOD]({commit,state},{_class,_method,params,returnType,methodFullname})
    {
        if(state.hooks[_class] && state.hooks[_class][methodFullname])
            return
        const isHook = await state.socket.call('hookMethod',_class,_method,params,returnType,methodFullname)
        
        if(isHook)
        {
            commit(STORE_HOOK_METHOD,{
                _class,
                methodFullname
            });
        }
    },
    async [HOOK_CTOR]({commit,state},{_class,_ctor,params,ctorFullname})
    {
        if(state.hooks[_class] && state.hooks[_class][ctorFullname])
            return
        const isHook = await state.socket.call('hookCtor',_class,_ctor,params,ctorFullname)
        
        if(isHook)
        {
            commit(STORE_HOOK_CTOR,{
                _class,
                ctorFullname
            });
        }
    },

    async [UNHOOK]({commit,state},{_class,targetFullname})
    {
        if(state.hooks[_class] && state.hooks[_class][targetFullname])
        {
            const isUnhook = await state.socket.call('unhook',_class,targetFullname);
            if(isUnhook)
            {
                commit(REMOVE_HOOK_STATE,{
                    _class,
                    targetFullname
                })
            }
        }
    },

    async [MODIFY_METHOD]({commit,state},{_class,targetMethod,isModifyParams,newParams,isModifyReturn,newReturnType,newReturnVal,targetFullname})
    {
        if(state.modifyHooks[_class] && state.modifyHooks[_class][targetFullname])
            return
        const isHook = await state.socket.call('modifyMethod',_class,targetMethod,isModifyParams,newParams,isModifyReturn,newReturnType,newReturnVal,targetFullname);
        if(isHook)
        {
            commit(STORE_HOOK_MODIFY,{
                _class,
                targetFullname
            })
        }
    },

    async [UNHOOK_MODIFY]({commit,state},{_class,targetFullname})
    {
        if(state.modifyHooks[_class] && state.modifyHooks[_class][targetFullname])
        {
            const isUnhook = await state.socket.call('unhookModify',_class,targetFullname);
            if(isUnhook)
            {
                commit(REMOVE_HOOK_MODIFY_STATE,{
                    _class,
                    targetFullname
                })
            }
        }
    },
    async [UNLOAD_SYSTEM_SCRIPT]({commit,state},script)
    {
        
        if(state.systemScripts.includes(script))
        {
            const isUnload = await state.socket.call('unloadSystemScript',script);
            if(isUnload)
            {
                commit(REMOVE_SYSTEM_SCRIPT,{
                    script
                })
            }
        }

    },
    async [FETCH_SYSTEM_SCRIPTS]({commit,state})
    {
        const {scripts} = await state.socket.call('allSystemScripts');
        state.systemScripts = scripts;
    }
}