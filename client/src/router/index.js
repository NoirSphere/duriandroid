import Vue from 'vue'
import VueRouter from 'vue-router'
import Device from '@/views/Device/Device.vue'
import App from "@/views/Application/App.vue";
import Info from "@/views/Application/Info.vue";
import Class from "@/views/Application/Class.vue";
import File from "@/views/Application/File.vue";
import CodeRunner from "@/views/Application/CodeRunner.vue";
import Logcat from "@/views/Application/Logcat.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    name: 'Index',
    redirect: Device
  },
  {
    path: '/device',
    name: 'Device',
    component: Device,
    // children:[
    // ]
  },
  {
    path: '/device/:deviceID/app/:appName',
    name: 'App',
    component: App,
    redirect: { name: 'Info' },
    children: [
      {
        path: 'info',
        name: 'Info',
        component: Info
      },
      {
        path: 'class',
        name: 'Class',
        component: Class
      },
      {
        path: 'file',
        name: 'File',
        component: File
      },
      {
        path: 'code-runner',
        name: 'CodeRunner',
        component: CodeRunner
      },
      {
        path: 'logcat',
        name: 'Logcat',
        component: Logcat
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
