import Vue from 'vue'
import Main from './Main.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import moment from 'moment';


Vue.config.productionTip = false
Vue.prototype.moment = moment;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(Main)
}).$mount('#app')
