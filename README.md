![duriandroid](/client/src/assets/Duriandroid.png)

# Duriandroid

Duriandroid aims to be Android application security assessment platform.
Inspired by passionfruit project. (https://github.com/chaitin/passionfruit)

However, the developer (me) does not familiar with developing any program. 
Duriandroid is still buggy and I am sure that the code smell is so bad. 
Please kindly guide me to improve this project better.

To install:

> npm install -g duriandroid

To use:

> duriandroid [port]

by default, Duriandroid will use port `1337`