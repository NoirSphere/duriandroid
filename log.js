const {spawn} = require('child_process')

const appLog = spawn('adb',['logcat','-v','threadtime','--pid',5662])
appLog.stdout.on('data',(data)=>{
    var logChunk = String(data).split('\n')
    var formattedLog = []
    logChunk.forEach(log => {
        var logPart = log.split(/\s+/);
        var [date,time,pid,tid,tag,...rest] = logPart;
        rest = rest.join(' ')
        console.log(rest.split(/\s*:\s*/,2));
        
        
        //no message
        // if(logPart.length >= 6)
        // {
        //     formattedLog.push(
        //         {
        //             date, 
        //             time, 
        //             pid, 
        //             tid, 
        //             tag, 
        //             issuer, 
        //             'message' : message.join(' ')
        //         }
        //     ) 
        // }
    });
    // console.log(formattedLog);
    
})
appLog.stderr.on('data',(data)=>{
    console.error(`stderr ${data}`)
})
appLog.on('close',(code)=>{
    console.log(`adb logcat exit with with code ${code}`)
})