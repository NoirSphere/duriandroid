const socketIO = require('socket.io')
const io = socketIO({ path: '/io' })
const path = require('path')
const client = io.of('client')
const deviceIO = require('./device')(io)
const { RPCScript } = require('./script.js')
const frida = require('frida')
const { execSync, spawn } = require('child_process')

client.on('connection', async (socket) => {
    console.log('a user connected');

    const { device, appPackagename } = socket.handshake.query;
    console.log(device, appPackagename);

    if (!device || !appPackagename) {
        console.error(device, appPackagename);
        socket.disconnect(true);
        return
    }

    let appSession, dev, app;
    try {
        dev = await frida.getDevice(device);
        const apps = await dev.enumerateApplications();
        app = apps.find(app => app.identifier === appPackagename);
        if (!app) throw new Error('App not exist!');


        socket.emit('app', {
            device: dev,
            app: app
        })

        let pid;
        if (app.pid) {
            const front = await dev.getFrontmostApplication()
            if (!front || (front.pid !== app.pid)) {
                await dev.kill(app.pid)
                pid = await dev.spawn(app.identifier)
                dev.resume(pid)
            }
        } else {
            pid = await dev.spawn(app.identifier)
            dev.resume(pid)
        }
        appSession = await dev.attach(app.identifier)
        appSession.detached.connect((reason) => {
            console.log('detached', reason);

            socket.emit('detached', reason)
            socket.disconnect(true)
        })

        socket.on('app', async (data, ack) => {
            try {
                ack({
                    status: 'ok',
                    data: apps.find(app => app.identifier === appPackagename)
                })
            } catch (error) {
                ack({
                    status: 'error',
                    error: `${error}`
                })
            }
        })
        await appSession.enableJit();
        var scriptHandler = new RPCScript(appSession, socket);
        scriptHandler.handleSocket();
        //Use this to run default RPC and send signal when ready
        // await userScript.loadDefaultRPC()
        socket.emit('ready');






    } catch (error) {
        console.error(error);
        socket.disconnect(true)
    }

    var appLog;
    //listen to client
    socket
        .on('disconnect', async () => {
            await appSession.detach();
            console.log('user disconnected');
        })
        .on('img',async (filePath,ack) => {
            try {
                res = await execSync(`adb -s ${dev.id} shell cat ${filePath}`, { encoding: 'binary' })
                ack({
                    status: 'ok',
                    data: new Buffer(res,'binary')
                })
            } catch (error) {
                ack({
                    status: 'error',
                    error: `${error}`
                })
            }
        })
        .on('cat',async (filePath,ack)=>{
            try {
                res = await execSync(`adb -s ${dev.id} shell cat ${filePath}`, { encoding: 'utf-8' })
                ack({
                    status: 'ok',
                    data: res
                })
            } catch (error) {
                ack({
                    status: 'error',
                    error: `${error}`
                })
            }
        })
        .on('ls', async (dir, ack) => {
            try {
                var res;
                res = await execSync(`adb -s ${dev.id} shell ls -alh ${dir}`, { encoding: 'utf-8' })
                var [total,...result_chunk] = String(res).split('\n');
                var dirs = [];
                result_chunk.forEach(row => {
                    var rowPart = row.split(/\s+/);
                    if(rowPart.length >= 9)
                    {
                        var [perm,linksNo,owner,group,size,date,time,name,...link] = rowPart;
                        let filePath = ""
                        
                        if(link.length > 1)
                        {
                            filePath = link[1]
                            link = link.join(" ")
                        }else{
                            filePath = path.posix.join(String(dir),String(name)) 
                        }
                        if(name == '.' || name == '..')
                        return ;
                        var isDir = false;
                        if(['d','l'].includes(perm.charAt(0)))
                        {
                            isDir=true;
                        }
                        dirs.push({
                            isDir,
                            name,
                            perm,
                            owner,
                            linksNo,
                            group,
                            size,
                            date,
                            time,
                            link,
                            path:filePath,
                            extension : path.posix.extname(filePath)
                        })
                    }
                })

                ack({
                    status: 'ok',
                    data: {
                        pwd : dir,
                        dirs
                    }
                })
            } catch (error) {
                ack({
                    status: 'error',
                    error: `${error}`
                })
            }
        })

        .on('logcat', (packageName, ack) => {
            try {
                appLog = spawn('adb', ['-s',dev.id,'logcat', '--pid', appSession.pid])
                appLog.stdout.on('data', (data) => {
                    var logChunk = String(data).split('\n')
                    var formattedLog = []
                    logChunk.forEach(log => {
                        var logPart = log.split(/\s+/);
                        if (logPart.length < 6)
                            return
                        var [date, time, pid, tid, tag, issuer, ...message] = logPart
                        //no message
                        formattedLog.push(
                            {
                                date,
                                time,
                                pid,
                                tid,
                                tag,
                                'issuer': issuer.replace(':', ''),
                                'message': message.join(' ')
                            }
                        )
                    });
                    socket.emit('log', {
                        subject: 'message',
                        data: formattedLog
                    })
                })
                appLog.stderr.on('data', (data) => {
                    socket.emit('log', {
                        subject: 'error',
                        data: String(data)
                    })
                    console.error(`stderr ${data}`)
                })
                appLog.on('close', (code) => {
                    socket.emit('logClose', code);
                    console.log(`adb logcat exit with with code ${code}`)
                })
                ack({
                    status: 'ok',
                    data: 'logcat started'
                })
            } catch (error) {
                ack({
                    status: 'error',
                    error: `${error}`
                })
            }
        })
        .on('stopLogcat', (data, ack) => {
            try {
                appLog.kill();
                ack({
                    status: 'ok',
                    data: 'logcat is closed'
                })
            } catch (error) {
                ack({
                    status: 'error',
                    error: `${error}`
                })
            }
        })
        .on('detach', (data, ack) => {
            try {
                ack({
                    status: 'ok',
                    data: 'app detached'
                })
                socket.disconnect();
            } catch (error) {
                ack({
                    status: 'error',
                    error: `${error}`
                })
            }

        })
        .on('kill', async (ack) => {
            const pid = appSession.pid;
            await appSession.detach();
            await dev.kill(pid);
            ack({
                status: 'ok',
                data: { message: `${appPackagename} has been terminated` }
            });
            socket.disconnect();

        })
    // userScript(client,)
})

// exports.attach = server => io.attach(server)
// exports.broadcast = client.emit.bind(client)

module.exports =
{
    'device': deviceIO,
    // 'userScript' : userScript,
    'attach': server => io.attach(server),
    'broadcast': client.emit.bind(client)
}