const frida = require('frida')
exports = module.exports = (io) =>
{
    const deviceIO = io.of('device')
    const deviceManager = frida.getDeviceManager()
    deviceManager.added.connect(async device => {
        console.log('added');
        deviceIO.emit('add',device.impl)
    })
    deviceManager.removed.connect(async device => {
        console.log('removed');
        deviceIO.emit('remove',device.impl)
    })
}




