// var uuid = require('uuid').v4
var util = require('util')
var fs = require('fs')
var path = require('path')
var readFile = util.promisify(fs.readFile)
class RPCScript {
    constructor(appSession, socket) {
        this.appSession = appSession;
        this.socket = socket;
        this.scripts = {};
        this.systemScripts = {};
        this.hooks = {};
        this.modifyHook = {};
    }

    handleSocket() {
        this.socket.on('runScript', this.wrap('runScript'));
        this.socket.on('destroyScript', this.wrap('destroyScript'));
        this.socket.on('allScripts', this.wrap('allScripts'));
        this.socket.on('clearScript', this.wrap('clearScript'));
        this.socket.on('appInfo', this.wrap('appInfo'));
        this.socket.on('appClasses', this.wrap('appClasses'));
        this.socket.on('appClass', this.wrap('appClass'));
        this.socket.on('dismissClass', this.wrap('dismissClass'));
        this.socket.on('hookCtor', this.wrap('hookCtor'));
        this.socket.on('hookMethod', this.wrap('hookMethod'));
        this.socket.on('unhook', this.wrap('unhook'));
        this.socket.on('modifyMethod', this.wrap('modifyMethod'));
        this.socket.on('unhookModify', this.wrap('unhookModify'));
        this.socket.on('killSSL', this.wrap('killSSL'));
        this.socket.on('bypassRoot', this.wrap('bypassRoot'));
        this.socket.on('unloadSystemScript', this.wrap('unloadSystemScript'));
        this.socket.on('allSystemScripts', this.wrap('allSystemScripts'));
    }

    wrap(event) {
        const method = this[event].bind(this);

        return async (data, ack) => {
            try {
                ack({
                    status: 'ok',
                    data: await method(...data)
                })
            } catch (error) {
                console.error('Error', error.stack || error);
                console.error('method:', event, 'args:', 'data');
                ack({
                    status: 'error',
                    error: `${error}`
                })
            }
        }
    }

    async killSSL()
    {
        try {
            const script = await this.loadAgentScript('killssl_full');
            const result = await script.exports.killSSL();
            this.systemScripts['killssl_full'] = script;
            return result;
        } catch (error) {
            throw new Error(error.stack || error);
        }
    }
    async bypassRoot()
    {
        try {
            const script = await this.loadAgentScript('root_bypass');
            const result = await script.exports.bypassRoot();
            this.systemScripts['root_bypass'] = script;
            return result;
        } catch (error) {
            throw new Error(error.stack || error);
        }
    }

    async runScript(fileName, userCodeFile) {
        const rpcScript = (`
        rpc.exports.bootstrap = function(js) {
            // temp workaround for
            // https://github.com/frida/frida-node/pull/28
            // in case the output goes to server side console instead
            // being sent to frontend
            //
            // this is not a sandbox, do not waste your time on escaping it

            ['log', 'warn', 'error'].forEach(function(level) {
            console[level] = function() {
                send({
                subject: 'console.message',
                level: level,
                args: [].slice.call(arguments)
                });
            };
            });

            // wow, copied from frida-python
            try {
            const result = (1, eval)(js);
            if (result instanceof ArrayBuffer) {
                return result;
            } else {
                var type = (result === null) ? 'null' : typeof result;
                return [type, result];
            }
            } catch (e) {
            return ['error', e instanceof Error ? {
                name: e.name,
                message: e.message,
                stack: e.stack
            } : e + ''];
            }
        }
        `)
        const handleMessage = async (message, data) => {
            const { type, payload } = message

            this.socket.emit('script', {
                timestamp: Date.now(),
                subject: 'message',
                'filename': fileName,
                payload,
                data
            })
        }

        const handleDestroy = () => {
            this.socket.emit('script', {
                timestamp: Date.now(),
                subject: 'destroyed',
                'filename': fileName,
            })
        }
        const loadedScript = await this.loadScript(rpcScript, handleMessage, handleDestroy);
        this.scripts[fileName] = loadedScript;

        const result = await loadedScript.exports.bootstrap(userCodeFile);
        try {
            let [type, value] = result;
            if (result instanceof Buffer) {
                type = 'arraybuffer'
                value = Buffer.from(result).toString('base64')
            }
            if (type === 'error') {
                console.error('Uncaught user frida script', value.stack || value)
                throw new Error(value.stack || value);
            }
            return {
                'filename': fileName,
                type,
                value
            }
        } catch (error) {
            console.error('Uncaught user frida script', error.stack || error)
            throw new Error(error.stack || error);
        }
    }

    async destroyScript(fileName) {
        try {
            await this.scripts[fileName].unload();
            delete this.scripts[fileName];
            return {
                message: `${fileName} has been destroyed`,
                filename: fileName
            }

        } catch (error) {
            throw new Error(error.stack || error)
        }
    }

    async loadScript(selectedRPC, handleMessage = () => { }, handleDestroy = () => { }) {
        try {
            const script = await this.appSession.createScript(selectedRPC);
            script.message.connect(handleMessage)
            script.destroyed.connect(handleDestroy)
            await script.load();

            return script;
        } catch (error) {
            throw new Error(error.stack || error);
        }
    }

    async loadAgentScript(scriptName) {
        const rpcScript = await readFile(path.resolve(path.join(__basedir,'agent', 'rpc', `${scriptName}-rpc.js`)), 'utf-8');
        const handleMessage = (message, data) => {
            const { type, payload } = message;
            if (type == 'send') {
                console.log(`[${scriptName}]: `, payload)
                this.socket.emit('console', {
                    timestamp: Date.now(),
                    script: scriptName,
                    subject: 'message',
                    payload,
                    data
                })
            } else if (type == 'error') {
                const { description, stack, lineNumber, columnNumber } = message;
                console.error(`[${scriptName}]: `, stack, `line:${lineNumber} column:${columnNumber}`);
                this.socket.emit('console', {
                    timestamp: Date.now(),
                    script: scriptName,
                    subject: 'error',
                    description
                })
            }
        }

        const handleDestroy = () => {
            console.log(`[${scriptName}]: destroyed`)
            this.socket.emit('console', {
                timestamp: Date.now(),
                script: scriptName,
                subject: 'destroyed',
            })
        }
        const loadedScript = await this.loadScript(rpcScript,handleMessage,handleDestroy);
        return loadedScript;
    }

    async appInfo() {
        try {
            const script = await this.loadAgentScript('metadata');
            const result = await script.exports.getInfo();
            await script.unload();
            return result;

        } catch (error) {
            throw new Error(error.stack || error);
        }
    }

    async appClasses() {
        try {
            const script = await this.loadAgentScript('class');
            const result = await script.exports.enumerateClasses();
            this.systemScripts['class'] = script;
            return result;
        } catch (error) {
            throw new Error(error.stack || error);
        }
    }

    async appClass(className) {
        try {
            const script = this.systemScripts['class'] || await this.loadAgentScript('class');
            const result = await script.exports.getClass(className);
            return result;
        } catch (error) {
            throw new Error(error.stack || error);
        }
    }

    async dismissClass() {
        try {
            if (this.systemScripts['class']) {
                await this.systemScripts['class'].unload();
                delete this.systemScripts['class'];
                return {
                    message: 'Unhook class inspection script',
                }
            }

        } catch (error) {
            throw new Error(error.stack || error)
        }
    }

    async hookCtor(_class, _ctor, params, ctorFullname) {
        //TODO: Deals with Lambda function and function that contain $ 
        try {
            const hookRPC = `
            rpc.exports.hook = function()
            {
                if(Java.available)
                {
                    Java.perform(function () 
                    {
                        try
                        {
                            var targetClass = Java.use('@targetClass@');
                            var logArgs = {'type':'arg',args:[]};
                            var logRet = {'type':'return','return':{}};
                            targetClass.$init.overload(@overload@).implementation = function(@params@)
                            {
                                //log params part
                                @logArgs@
    
                                //call function
                                send(logArgs);
                                this.$init(@params@);
                            }
                        } catch(error) {
                            console.error(error);
                        }
                    });
            
                }
            }
            `;

            var code = hookRPC.replace(/@targetClass@/g, _class);
            var overload = '';
            var paramsFrida = '';
            var logArgs = '';
            var initArgsClass = '';
            params.forEach((param, idx) => {
                //TODO: Deal with array params (start with [L )
                if (param.type == '[B') {
                    logArgs = logArgs + `
                    var ${param.name}_log = '';
                    for (var i=0; i < ${param.name}.length; i++)
                    {
                        ${param.name}_log += String.fromCharCode(${param.name}[i]);
                    }\n
                    logArgs['args'].push({
                        'name':'${param.name}',
                        'value': {
                            'raw' : JSON.stringify(${param.name}),
                            'string' : ${param.name}_log
                        }
                    });\n
                    `;
                } else {
                    logArgs = logArgs + `
                    logArgs['args'].push({
                        'name':'${param.name}',
                        'value': {
                        'string': String(${param.name})
                        }
                    });\n
    
                    `

                }
                // logArgs = logArgs + `send('name: ',${arg.name},'type',${arg.type},'value',String(${arg.name}))\n`

                overload = overload + `'${param.type}'`
                paramsFrida = paramsFrida + `${param.name}`
                if (idx < (params.length - 1)) {
                    overload = overload + ','
                    paramsFrida = paramsFrida + ','
                }
            })
            code = code.replace(/@initArgsClass@/g, initArgsClass);
            code = code.replace(/@logArgs@/g, logArgs);
            code = code.replace(/@overload@/g, overload);
            code = code.replace(/@params@/g, paramsFrida);

            const handleMessage = (message, data) => {
                console.log(message);

                const { type, payload } = message;
                if (type == 'send') {
                    console.log(`[hook constructor]: `, payload)
                    this.socket.emit('console', {
                        timestamp: Date.now(),
                        script: 'hook_ctor',
                        subject: 'message',
                        _class,
                        ctor: _ctor,
                        payload,
                        data
                    })
                } else if (type == 'error') {
                    const { description, stack, lineNumber, columnNumber } = message;
                    console.error(`[hook constructor]: `, stack, `line:${lineNumber} column:${columnNumber}`);
                    this.socket.emit('console', {
                        timestamp: Date.now(),
                        script: 'hook_ctor',
                        subject: 'error',
                        _class,
                        ctor: _ctor,
                        description
                    })
                }
            }

            const handleDestroy = () => {
                console.log(`[hook constructor]: destroyed`)
                this.socket.emit('console', {
                    timestamp: Date.now(),
                    script: 'hook_ctor',
                    subject: 'destroyed',
                    _class,
                    ctor: _ctor,
                })
            }
            const script = await this.loadScript(code, handleMessage, handleDestroy);

            if (this.hooks.hasOwnProperty(_class)) {
                this.hooks[_class][ctorFullname] = script;
            }
            else {
                this.hooks[_class] = {
                    [ctorFullname]: script
                };
            }

            await script.exports.hook();

            return true;
        } catch (error) {
            throw new Error(error || error.stack)
        }
    }

    async hookMethod(_class, _method, params, returnType, methodFullname) {

        try {
            const hookRPC = `
            rpc.exports.hook = function()
            {
                if(Java.available)
                {
                    Java.perform(function () 
                    {
                        try
                        {
                            var targetClass = Java.use('@targetClass@');
                            var logArgs = {'type':'arg','args':[]};
                            var logRet = {'type':'return','return':{}};
                            //  @initArgsClass@
                            targetClass.@targetFunction@.overload(@overload@).implementation = function(@params@)
                            {
                                logArgs.args = [];
                                //log params part
                                @logArgs@
    
                                //call function
                                send(logArgs);
                                var ret = this.@targetFunction@.overload(@overload@).call(this,@params@);
                                @returnTypeToString@
                                send(logRet);
                                return ret;
                            }
                        } catch(error) {
                            console.error(error);
                        }
                    });
            
                }
            }
            `;

            var code = hookRPC.replace(/@targetClass@/g, _class);
            code = code.replace(/@targetFunction@/g, _method);
            var initArgsClass = ''
            var overload = '';
            var paramsFrida = '';
            var logArgs = '';
            params.forEach((param, idx) => {
                initArgsClass = initArgsClass + `var _${param.name} = Java.use('${param.type}');\n`
                if (param.type == '[B') {
                    logArgs = logArgs + `
                    var ${param.name}_log = '';
                    for (var i=0; i < ${param.name}.length; i++)
                    {
                        ${param.name}_log += String.fromCharCode(${param.name}[i]);
                    }\n
                    logArgs['args'].push({
                        'name':'${param.name}',
                        'value':{
                            'raw' : JSON.stringify(${param.name}),
                            'string' : ${param.name}_log
                        }
                    });\n
                    `;
                } else {
                    logArgs = logArgs + `
                    logArgs['args'].push({
                        'name':'${param.name}',
                        'value' :{
                        'string':JSON.stringify(${param.name})
                        }
                    });\n
    
                    `
                }
                // logArgs = logArgs + `send('name: ',${arg.name},'type',${arg.type},'value',String(${arg.name}))\n`

                overload = overload + `'${param.type}'`
                paramsFrida = paramsFrida + `${param.name}`
                if (idx < (params.length - 1)) {
                    overload = overload + ','
                    paramsFrida = paramsFrida + ','
                }
            })
            var returnTypeToString = '';
            if (returnType == '[B') {
                returnTypeToString = `
                var ret_log = '';\n
                for (var i=0; i < ret.length; i++)
                {
                    ret_log += String.fromCharCode(ret[i]);
                }\n
                logRet['return'] = {
                    'raw' : JSON.stringify(ret),
                    'string' : ret_log
                }\n
                `;
            } else if (returnType != 'void') {

                returnTypeToString = `
                
                logRet['return'] = {
                    'string':String(ret)
                };\n
                
                `
            }

            code = code.replace(/@returnTypeToString@/g, returnTypeToString);
            code = code.replace(/@logArgs@/g, logArgs);
            code = code.replace(/@overload@/g, overload);
            code = code.replace(/@params@/g, paramsFrida);

            const handleMessage = (message, data) => {
                console.log(message);

                const { type, payload } = message;
                if (type == 'send') {
                    console.log(`[hook_method] ${_class} at ${_method}: `, payload)
                    this.socket.emit('console', {
                        timestamp: Date.now(),
                        script: 'hook_method',
                        subject: 'message',
                        _class,
                        method: _method,
                        payload,
                        data
                    })
                } else if (type == 'error') {
                    const { description, stack, lineNumber, columnNumber } = message;
                    console.error(`[hook_method]: `, stack, `line:${lineNumber} column:${columnNumber}`);
                    this.socket.emit('console', {
                        timestamp: Date.now(),
                        script: 'hook_method',
                        subject: 'error',
                        _class,
                        method: _method,
                        description
                    })
                }
            }

            const handleDestroy = () => {
                console.log(`[hook] ${_class} at ${_method}: destroyed`)
                this.socket.emit('console', {
                    timestamp: Date.now(),
                    script: 'hook_method',
                    subject: 'destroyed',
                    _class,
                    method: _method,
                })
            }
            const script = await this.loadScript(code, handleMessage, handleDestroy);

            if (this.hooks.hasOwnProperty(_class)) {
                this.hooks[_class][methodFullname] = script;
            }
            else {
                this.hooks[_class] = {
                    [methodFullname]: script
                };
            }

            await script.exports.hook();

            return true;
        } catch (error) {
            throw new Error(error || error.stack)
        }
    }

    async modifyMethod(_class, targetMethod, isModifyParams, newParams, isModifyReturn, newReturnType, newReturnVal = '', targetFullname) {
        try {
            console.log(_class, targetMethod, isModifyParams, newParams, isModifyReturn, newReturnType, newReturnVal);

            var modifyMethodRPC = `
            rpc.exports.modifyMethod = function()
            {
                if(Java.available)
                {
                    Java.perform(function () 
                    {
                        try
                        {
                            var targetClass = Java.use('@targetClass@');
                            var logArgs = {'type':'arg',args:[]};
                            var logRet = {type:'return',return:{}};
                            targetClass.@targetFunction@.overload(@overload@).implementation = function(@params@)
                            {
                                logArgs.args = [];
                                @newParamsType@
                                @initNewParams@
                                //log new value
                                @logArgs@
                                //return val
                                @callMethod@
                                @returnType@
                                @initNewReturn@
                                @returnTypeToString@
                                @returnNewVal@
                            }
                        } catch(error) {
                            console.error(error);
                        }
                    });
            
                }
            }
            `;

            var primitiveType = ['int', 'boolean', 'long', 'char', 'short', 'byte', 'double', 'void']
            var code = ''
            var newParamsType = '';
            var initNewParams = ''
            var overload = '';
            var paramsFrida = '';
            var logArgs = '';
            var callMethod = '';
            var returnTypeToString = '';
            var returnNewVal = '';


            newParams.forEach((param, idx) => {
                // initArgsClass = initArgsClass + `var _${param.name} = Java.use('${param.type}');\n`
                if (isModifyParams) {
                    if (param.isModify) {
                        if (!primitiveType.includes(param.type)) {
                            throw new Error('Non-primitive or array type is not supported right now')
                        }
                        //TODO: store prev value
                        // initNewParams = initNewParams + `var prev_${param.name} = ${param.name};\n`;
                        initNewParams = initNewParams + `${param.name} = ${param.value};\n`;
                    }
                    if (param.type == '[B') {
                        logArgs = logArgs + `
                        var ${param.name}_log = '';
                        for (var i=0; i < ${param.name}.length; i++)
                        {
                            ${param.name}_log += String.fromCharCode(${param.name}[i]);
                        }\n
                        logArgs['args'].push({
                            'name':'${param.name}',
                            'value':{
                                'raw' : JSON.stringify(${param.name}),
                                'string' : ${param.name}_log
                            },
                            'isModify' : ${param.isModify},
                        });\n
                        `;
                    } else {
                        logArgs = logArgs + `
                        logArgs['args'].push({
                            'name':'${param.name}',
                            'value' :
                            {
                                'string':JSON.stringify(${param.name})
                            },
                            'isModify' : ${param.isModify}
                        });\n
        
                        `
                    }
                }

                overload = overload + `'${param.type}'`
                paramsFrida = paramsFrida + `${param.name}`
                if (idx < (newParams.length - 1)) {
                    overload = overload + ','
                    paramsFrida = paramsFrida + ','
                }
            }, '')
            if (isModifyParams) {
                logArgs = logArgs + 'send(logArgs);\n';
                if (!isModifyReturn) {

                    callMethod = 'var ret = this.@targetFunction@.overload(@overload@).call(this,@params@);\n'
                    if (newReturnType == '[B') {
                        returnTypeToString = `
                        var ret_log = '';\n
                        for (var i=0; i < ret.length; i++)
                        {
                            ret_log += String.fromCharCode(ret[i]);
                        }\n
                        logRet['return'] = {
                            'raw' : JSON.stringify(ret),
                            'string' : ret_log
                        }\n
                        `;
                    } else if (newReturnType != 'void') {

                        returnTypeToString = `
                        
                        logRet['return'] = {
                            'string':String(ret)
                        };\n
                        
                        `
                    }
                    returnTypeToString = returnTypeToString + 'send(logRet);\nreturn ret;'
                    returnNewVal = 'return ret;\n'
                }
            }
            code = modifyMethodRPC.replace(/@newParamsType@/g, newParamsType)
            code = code.replace(/@logArgs@/g, logArgs);
            code = code.replace(/@callMethod@/g, callMethod)
            code = code.replace(/@initNewParams@/g, initNewParams);
            code = code.replace(/@overload@/g, overload);
            code = code.replace(/@params@/g, paramsFrida);
            code = code.replace(/@targetClass@/g, _class);
            code = code.replace(/@targetFunction@/g, targetMethod);
            var returnClass = '';
            var initNewReturn = '';
            if (isModifyReturn) {
                if (primitiveType.includes(newReturnType)) {
                    // var returnClass = `var _class = Java.use('java.lang.Class');
                    //                     var returnClass = _class.getPrimitiveClass('${typeDenote}')`;
                    returnClass = '';
                    initNewReturn = `var newReturn = ${newReturnVal}`
                    returnTypeToString = `
                    
                    logRet['return'] = {
                        'string': String(newReturn)
                    };\n
                    
                    `

                } else {
                    throw new Error('Non-primitive or array type is not supported right now')
                    returnClass = `var returnClass = Java.use('${newReturnType}')`;
                    initNewReturn = `Java.cast()`
                }
                returnNewVal = 'send(logRet);\nreturn newReturn\n';
            }
            code = code.replace(/@returnNewVal@/g, returnNewVal);
            code = code.replace(/@returnType@/g, returnClass);
            code = code.replace(/@initNewReturn@/g, initNewReturn);
            code = code.replace(/@returnTypeToString@/g, returnTypeToString);

            const handleMessage = (message, data) => {
                console.log(message);

                const { type, payload } = message;
                if (type == 'send') {
                    console.log(`[modify method]: `, payload)
                    this.socket.emit('console', {
                        timestamp: Date.now(),
                        script: 'modify_method',
                        subject: 'message',
                        _class,
                        method: targetMethod,
                        payload,
                        data
                    })
                } else if (type == 'error') {
                    const { description, stack, lineNumber, columnNumber } = message;
                    console.error(`[modify method]: `, stack, `line:${lineNumber} column:${columnNumber}`);
                    this.socket.emit('console', {
                        timestamp: Date.now(),
                        script: 'modify_method',
                        subject: 'error',
                        _class,
                        method: targetMethod,
                        description
                    })
                }
            }

            const handleDestroy = () => {
                console.log(`[modify method]: destroyed`)
                this.socket.emit('console', {
                    timestamp: Date.now(),
                    script: 'modify_method',
                    subject: 'destroyed',
                    _class,
                    method: targetMethod,
                })
            }
            console.log(code);

            const script = await this.loadScript(code, handleMessage, handleDestroy);

            if (this.modifyHook.hasOwnProperty(_class)) {
                this.modifyHook[_class][targetFullname] = script;
            }
            else {
                this.modifyHook[_class] = {
                    [targetFullname]: script
                };
            }


            const res = await script.exports.modifyMethod();
            return true;
        } catch (error) {
            console.error(code);

            throw new Error(error || error.stack);
        }
    }
    async unloadSystemScript(script)
    {
        await this.systemScripts[script].unload();
        delete this.systemScripts[script];
        return true;
    }
    async unhook(_class, targetFullname) {
        await this.hooks[_class][targetFullname].unload();
        delete this.hooks[_class][targetFullname];
        if (Object.keys(this.hooks[_class]).length <= 0) {
            delete this.hooks[_class];
        }
        return true;
    }
    async unhookModify(_class, targetFullname) {
        await this.modifyHook[_class][targetFullname].unload();
        delete this.modifyHook[_class][targetFullname];
        if (Object.keys(this.modifyHook[_class]).length <= 0) {
            delete this.modifyHook[_class];
        }
        return true;
    }



    allScripts() {
        return { scripts: Object.keys(this.scripts) };
    }
    allSystemScripts() {
        return { scripts: Object.keys(this.systemScripts) };
    }

    clearScript() {
        for (const script in this.scripts) {
            try {
                delete this.scripts[script];
            } catch (error) {
                throw new Error(`Problem deleting ${script}`)
            }
        }
    }
}

module.exports = {
    RPCScript: RPCScript
}