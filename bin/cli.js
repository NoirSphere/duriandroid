#!/usr/bin/env node

const server = require('../server')

function usage() {
  console.log('usage: duriandroid [port]')
  process.exit(-1)
}


function main() {
  if (process.argv.length > 2) {
    const port = parseInt(process.argv[2])
    if (port === NaN)
      return usage()
    return server.start(port);
  }
  return server.start(1337)
}

main()
