const nodemon = require('nodemon')
const {spawn} = require('child_process')

process.env.NODE_ENV = 'development'

//server

nodemon({
    script: 'server.js',
    ext:'js json',
    watch:['server.js','agent']
})

nodemon 
.on('start', () => console.log('SERVER started'))
.on('restart', files => console.log('App restarted due to: ', files))
.on('quit', () => {
    console.log('Server has been terminated.')
    process.exit()
  })

const webpack = spawn('npm', ['run', 'serve'], {
    cwd: 'client',
    shell: true,
    stdio: 'inherit',
})

webpack.on('exit',()=>{
    console.warn('web terminated');
    process.exit()
})

process.on('exit',()=>{
    if(!webpack.killed) webpack.kill()
})